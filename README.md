# README #
This projects contains all sorts of script for all sorts of backups.

### Git backup

The utility is the **git** folder. The command takes a configuration file that contains a list of
git repositories (one per line). For example

```text
git@gitlab.com:netfeverteam/backup-solutions/scripts.git
git@gitlab.com:netfeverteam/libs/yoz-commons.git
```

```shell
./git/bin/mirror-git-repositories <config_file_path>
```

### Useful tips for backuping ###

To install public key authentication as explained at https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2

``` 
rsync -avz -delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress /path/to/backup user@1.2.1.2:~/path/to/folder
```
