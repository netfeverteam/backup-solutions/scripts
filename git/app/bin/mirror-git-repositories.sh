#!/bin/bash
# Proper header for a Bash script.
CONF_FILE="$1"
ID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
LOG_PREFIX="[BK][GIT-MIRROR][$ID]"

DATE=$(date +%Y-%m-%d)
BASH_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
LOG_FOLDER=$BASH_FOLDER/../log/$DATE

info () {
	logger "$LOG_PREFIX[INFO] $1"
	echo "$1"
}

error () {
        logger "$LOG_PREFIX[ERROR] $1"
        echo "$1"
}

if [ $# -eq 0 ]; then
    echo "No arguments provided"
    exit 1
fi

info "mirroring git from $1"

mkdir -p $LOG_FOLDER

while read -r line || [[ -n "$line" ]]; do
	LOG_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)
	CMD="GIT_URL=$line $BASH_FOLDER/mirror-git-repository.sh | tee $LOG_FOLDER/$LOG_NAME.log"

	set -o pipefail

	eval "$CMD"
	retVal=$?

	if [ $retVal -ne 0 ]; then
            	error "Error processing $line"
	else
                rm $LOG_FOLDER/$LOG_NAME.log
	fi
done < "$CONF_FILE"
