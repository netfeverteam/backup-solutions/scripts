#!/bin/bash
# Proper header for a Bash script.

ID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
PROJECT=$(basename $GIT_URL)
LOG_PREFIX="[BK][GIT-MIRROR][$ID]"

info () {
	logger "$LOG_PREFIX[INFO] $1"
	echo "$1"
}

error () {
        logger "$LOG_PREFIX[ERROR] $1"
        echo "$1"
}


if [ -z "$GIT_URL" ]
then
        error "GIR_URL is undefined, terminating"
        exit 1
fi

PROJECT_NAME=$(basename $GIT_URL)

info "Running GIT Mirror Script Start"
info "Project: $PROJECT_NAME"
info "URL: $GIT_URL"

if [ -z "$GIT_SRC_DIR" ]
then
	info "GIT_SRC_DIR is undefined, default to ~/src"
	export GIT_SRC_DIR=~/src
fi

if [ ! -d $GIT_SRC_DIR ]; then
    	info "Creating folder $GIT_SRC_DIR"
	mkdir -p $GIT_SRC_DIR
fi

if [ ! -d $GIT_SRC_DIR/$PROJECT_NAME ]; then
        info "$PROJECT_NAME not found, do a clone"
        cd $GIT_SRC_DIR
	info "running cd $GIT_SRC_DIR && git clone --mirror $GIT_URL"
	git clone --mirror $GIT_URL
else
        info "Updating $PROJECT_NAME"
        cd $GIT_SRC_DIR/$PROJECT_NAME
	info "running cd $GIT_SRC_DIR/$PROJECT_NAME && git remote update"
	git remote update
fi

retVal=$?
if [ $retVal -ne 0 ]; then
	error "An error occured running the script"
	exit $retVal
	echo "1232142315325325325325"
else 
	info "success"
fi
